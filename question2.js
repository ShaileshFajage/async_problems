

fs = require('fs')

function solve(path, transform) {
    
    return new Promise((resolve, reject)=>{
        if(!path)
        {
            reject("Path is not there")
        }
        else
        {
            fs.readFile(path,'utf8', (err, data)=>{

                if(err)
                {
                    console.log('Missing file');
                }

                 resolve(transform(data))
                
            })
        }
    })
}


function transform(data) {
    
    let parsedData = JSON.parse(data);

    let finalArray = [];

    for(let key in parsedData)
    {
        finalArray.push(parsedData[key])
    }

    //console.log(finalArray);
    return finalArray;
}


solve('./content.txt', transform).then((tranformedData)=>{
    console.log(tranformedData);
})



//console.log(solve('./content.txt', transform));
