

// Q4. Write a function that takes any number of arguments (userIds) ..and makes api call to fetch the user details.

// A. use Promises.all and make api call for each user


let arr = [1,2]


function solve1(arr, url) {

    Promise.all(arr.map((id)=>{

        return fetch(url+"/"+id).then((data) => data.json() )
        
    })).then((res)=>{
        console.log(res);
    })
    

}


solve1(arr, 'https://jsonplaceholder.typicode.com/users')



// B. use only 1 api call to get all the results.

function solve2(arr,url) {
    url=url+'?'

    for(let i=0;i<arr.length-1;i++)
    {
        url = url+`id=${arr[i]}&`
    }

    url = url+`id=${arr[arr.length-1]}`;

    console.log(url);

    fetch(url).then(response => response.json())
    .then((res)=>{
        console.log(res);

    })
    
}





solve2(arr,'https://jsonplaceholder.typicode.com/users')




