
// Q1.Write a function to fetch list of all todos from the above url using both fetch and axios.get.

//import fetch from 'node-fetch'

//let fs = require('fs')

let resObj = {};


// A. use promises 

function fetchData1() {
    
   fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response)=>response.json())
    .then((data)=>{
         //console.log(data)
        resObj["A"] = data;
    })
    
}

fetchData1();


// B. use await keyword 


async function fetchData() {
    
    const data = await fetch('https://jsonplaceholder.typicode.com/todos')

    const result = await data.json();

    return result;
      
 }
 

//  C. Once the list is fetched.Group the list of tasks based on user IDs.

 fetchData().then((data)=>{

    resObj["B"] = data;
    
    let userIdBasedList = data.reduce((res, each)=>{

        if(res[each.userId])
        {
            if(each.completed)
            {
                res[each.userId].push(each)
            }
            else
            {
                res[each.userId].unshift(each)
            }
        }
        else
        {
            res[each.userId] = [];

            if(each.completed)
            {
                res[each.userId].push(each)
            }
            else
            {
                res[each.userId].unshift(each)
            }
            

        }

        return res;
    }, [])

    // console.log(userIdBasedList)
    resObj["C"] = userIdBasedList;



    // D.  Also Group tasks based on completed or nonCompleted for each user

    let taskBasedList = Object.keys(userIdBasedList).reduce((res, each)=>{

        let collect = userIdBasedList[each].reduce((collect, cur)=>{

            if(cur.completed)
            {
                
                if(collect["completed"])
                {
                    collect["completed"].push(cur)
                }
                else
                {
                    collect["completed"] = [];
                    collect["completed"].push(cur)
                }
            }
            else
            {
                if(collect["nonCompleted"])
                {
                    collect["nonCompleted"].push(cur)
                }
                else
                {
                    collect["nonCompleted"] = [];
                    collect["nonCompleted"].push(cur)
                }
            }

            return collect;
        },{})

        //console.log(collect);

        res[each] = collect;
        return res;

    },{})

    console.log(taskBasedList)

    resObj["D"] = taskBasedList;
     
 });

 //console.log(resObj);










