
const axios = require('axios').default;

const fs = require('fs')

var resObj = {};

axios.get('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.data)
    .then((data) => {

        resObj["A"] = data;

        resObj["B"] = groupBasedOnUserId(data);

        console.log(groupBasedOnUserId(data));

        resObj["C"] = groupBasedOnCompNoncomp(data);
        //console.log(resObj);

        fs.writeFile('./content.txt', JSON.stringify(resObj), (err) => {

            if (err) {
                throw err;
            }

        })


    })


    function groupBasedOnUserId(data) {
        
        let userIdBasedList = data.reduce((res, each) => {

            if (res[each.userId]) {
                if (each.completed) {
                    res[each.userId].push(each)
                }
                else {
                    res[each.userId].unshift(each)
                }
            }
            else {
                res[each.userId] = [];

                if (each.completed) {
                    res[each.userId].push(each)
                }
                else {
                    res[each.userId].unshift(each)
                }


            }

            return res;
        }, {})

        return userIdBasedList;
    }




    function groupBasedOnCompNoncomp(data) {

        let dataBasedId = groupBasedOnUserId(data)

        let taskBasedList = Object.keys(dataBasedId).reduce((res, each)=>{

            let collect = dataBasedId[each].reduce((collect, cur)=>{

                if(cur.completed)
                {
                    
                    if(collect["completed"])
                    {
                        collect["completed"].push(cur)
                    }
                    else
                    {
                        collect["completed"] = [];
                        collect["completed"].push(cur)
                    }
                }
                else
                {
                    if(collect["nonCompleted"])
                    {
                        collect["nonCompleted"].push(cur)
                    }
                    else
                    {
                        collect["nonCompleted"] = [];
                        collect["nonCompleted"].push(cur)
                    }
                }

                return collect;
            },{})

            //console.log(collect);

            res[each] = collect;
            return res;

        },{})

    //    console.log(taskBasedList);





        return taskBasedList;
    }









