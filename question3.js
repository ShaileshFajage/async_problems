

fetch('https://jsonplaceholder.typicode.com/users')
.then((response)=> response.json())
.then((data)=>{
    //console.log(data)

    let ids = data.reduce((res, each)=>{

        if(each.name.includes("Nicholas"))
        {
            res.push(each.id);
        }

        return res;
    },[])

    
    fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json())
    .then((data) => {

       let toDosOfNocolas = data.reduce((res, cur)=>{

        if(ids.includes(cur.id))
        {
            if(res.hasOwnProperty(cur.id))
            {
                res[cur.id].push(cur)
            }
            else
            {
                res[cur.id] = []
                res[cur.id].push(cur)
            }
            
        }

            //  console.log(res);
            return res;

        },{})


        console.log(toDosOfNocolas);
    })
})


